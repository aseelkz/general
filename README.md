# README #
This repository is my general, multi-use repo. 
 
Its primary purpose is to serve as a portfolio of sorts, 
 
for snippets, single-file projects, old assignments and 
 
code/documents I want to share with others. 



# Folders #

college - College assignments and documentation

VIITech - snippets from my internship at VII Technology Solutions
