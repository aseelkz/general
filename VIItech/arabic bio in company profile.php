@if(!empty($company->bio_ar))
	<div class=" columns small-12 company-profile-overview">
		<div class ="company-profile-about-arabic">
			{!! nl2br(e($company->bio_ar)) !!}
		</div><hr class = "expandable">
	</div>
@endif
