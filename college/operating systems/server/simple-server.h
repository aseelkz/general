#ifndef SIMPLE_NETWORK
#define SIMPLE_NETWORK

int start_server(int port);
int wait4request(int server);
int stop_server(int server);
int do_server_stuff(int client);
extern int done;

#endif
