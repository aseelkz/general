if(!$request->has( key: 'is_current')){
	$is_valid_date=$this->isValidYearMonth($request); 
}else{
	$today = Carbon::now();
	$request->merge(array('end_month' =>(string)$today->month));
	$request->merge(array('end_year'=> (string)$today->year));
}