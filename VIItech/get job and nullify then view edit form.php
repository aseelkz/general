/**
 * Show the form for editing the specified resource.
 * @param int $id 
 * @return \Illuminate\Http\Response
 */ 

public function edit($id)
{
	$job=Job::findOrFail($id);
	if($job->min_salary==0)
	{
		$job->min_salary=null;
	
	}

	if($job->max_salary==0)
	{
		$job->max_salary=null;
	}

	return view(view:$this->layout.'.edit', compact(varname: 'job'));
	
}