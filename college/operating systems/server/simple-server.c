#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <strings.h>
#include <sys/types.h>         
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "simple-server.h"

int done = 0;

int start_server(int port)
{
  int server;
  struct sockaddr_in serv_addr;

  /* Create a new TCP/IP socket */
  if (-1 == (server = socket(AF_INET, SOCK_STREAM, 0)))
    return -1;

  /* prepare the endpoint */
  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(port);

  /* Bind the endpoint to the socket */
  if (-1 == bind(server, (struct sockaddr *) &serv_addr, sizeof(serv_addr)))
    return -1;

  /* Prepare for listening */
  if (-1 == listen(server, 5))
    return -1;

  return server;
}

int wait4request(int server) {
  struct sockaddr_in cli_addr;
  socklen_t clilen = sizeof(cli_addr);

  /* Accept a request and get a new connection for it */
  return accept(server, (struct sockaddr *) &cli_addr, &clilen);
}


int stop_server(int server) {
  /* Close the socket */
  return close(server);
}

/* The actual server framework */
int main(int argn, char *argv[])
{
  int port = 8080;
  int server, client;
  char *end;

  /* Parse the command line arguments, if any */
  switch(argn) {
  case 1:			/* Use the default port */
    port = 8080;
    break;
  case 2:			/* Use the provided port */
    port = strtol(argv[1], &end, 10);
    if (*end) {
      fprintf(stderr, "USAGE: %s [port_no]\n", argv[0]);
      return EXIT_FAILURE;
    }
    break;
  default:
    fprintf(stderr, "USAGE: %s [port_no]\n", argv[0]);
    return EXIT_FAILURE;
  }

  /* Start a new server */
  if (-1 == (server = start_server(port))) {
    perror("Starting server");
    return EXIT_FAILURE;
  }

  /* The main loop of the server */
  do {
    /* Wait for the next request */
    if (-1 == (client = wait4request(server))) {
      perror("Accepting requests");
      return EXIT_FAILURE;
    }
    
    /* After receiving a request, do the server stuff */
    switch (do_server_stuff(client)) {
    case -1:
      return EXIT_FAILURE;
    case 0:
      break;
    default:
      break;
    }
  } while (!done);
  
  /* Finally,stop the server */
  if (-1 == stop_server(server)) {
    perror("Stopping server");
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
