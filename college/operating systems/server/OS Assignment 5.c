#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include <sys/shm.h> 
#include <sys/sem.h> 

void *checkcmd(int ctrl); 



int main(int argc, char* argv[])
{
	
	if(argc != 2){ perror("Incorrect number of arguments"); return -1; } //crash;  
	char *ptr; 
	int m, n; 
	m = strtol(argv[0], &ptr, 10); 
	n = strtol(argv[1], &ptr, 10); 

	if (m == NULL || n == NULL)
	{
		//error 
		perror("Arguments not in integer form"); 
		return -1; 
	}

	int memarray[m]; 
	//for each i=0,i<m, memarray[i] = shmget 

	for(int i= 0; i< m; i++)	
	{
		memarray[i] = shmget(IPC_PRIVATE, n, IPC_CREAT); 
		if(memarray[i] == -1 )
		{
			//handle error -------------------------------------------TODO 
		}
	}// end for 



	int mutex,empty,full; //semaphore array id 
	mutex = semget(IPC_PRIVATE, 1, IPC_CREAT); 
	if(mutex == -1)
	{
		perror("semget");
		return -1; 
	}
	
	empty= semget(IPC_PRIVATE, m, IPC_CREAT); 
	if(empty == -1)
	{
		perror("semget");
		return -1; 
	}

	full= semget(IPC_PRIVATE, 1, IPC_CREAT); 
	if(full == -1)
	{
		perror("semget");
		return -1; 
	} 

	int ctrl; 

	ctrl = semctl(mutex,1,SETVAL,1); 
	checkcmd(ctrl); 
	ctrl = semctl(full,1,SETVAL, 0); 
	checkcmd(ctrl); 
	ctrl = semctl(empty,1,SETVAL, m); 
	checkcmd(ctrl);





	int pid = fork(); 
	if(pid == -1)//error
	{
		perror("fork"); 
		return -1; 
	}
	else if(pid == 0 ) // child, consumer 
	{
		while(1)
		{
			sem_wait(mutex);
			sem_wait(empty);
			int num_empty = semctl(empty,1,GETVAL)-1; 
			checkcmd(num_empty);


			int write = fwrite(memarray[m-num_empty],1,n,stdout); 

			if(write<n)
			{
				break;
			}

			int ctrl = semctl(empty,1,SETVAL,num_empty+2);
			checkcmd(ctrl);  
			int fullval = semctl(full,1,GETVAL); 
			checkcmd(fullval); 

			if(fullval=1)
			{
				int fullval = semctl(full,1,SETVAL,0);
				checkcmd(fullval);
			}

			ctrl = sem_post(mutex); 
			checkcmd(ctrl); 

		}//end while loop
	}
	else ///  if(pid > 0) //parent, producer 
	{
		while(1)
		{
			//get dat from stdin


			////check for mutex and full, wait
			//lock mutex
			sem_wait(mutex); 
			sem_wait(full); 


			//write into memarry[n-empty] ////(if empty wrtite into n-n = 0, if 3/6 empty write into n-empty = 6-3 =3 
			int num_empty = semctl(empty,1,GETVAL)+1;
			checkcmd(num_empty); 
			int read = fgets(memarray[m-num_empty],n,stdin); 

			if(read<n)
			{
				// handle - last segment //////////////////////////////////////////////////////
			}


			//if empty == 0, full = 1 
			if (num_empty == 0)
			{
				int ctrl = semctl(full,1,SETVAL,1);
				checkcmd(ctrl);
			}
			

			//unlock mutex 
			int ctrl = sem_post(mutex); 
			checkcmd(ctrl); 


		}//end while
	}


	return 1; 
}//end main



void* checkcmd(int ctrl)
{
	if(ctrl == -1)
	{
		perror("semctl"); 
		exit(-1);
	}
}