#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include "simple-server.h"
#define BUF_SIZE 4096
int total =0; 
int current = 0; 
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER; 

void print_stuff();
int parsehttp(char[], char*);
int get_request(int client);




int do_server_stuff(int client)
{
  int rc;
  pthread_t      tid;  // thread ID
  pthread_attr_t attr; // thread attribute


  // set thread detachstate attribute to DETACHED 
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
  // create the thread 
  rc = pthread_create(&tid, &attr, get_request(client), NULL);
  return rc; 

} //end do server stuff 



void print_stuff()
{
  puts("printing from pthread");
  return; 
} //end print stuff 



int get_request(int client)
{

  char buffer[BUF_SIZE];
  int n; 


  /* Get the request; BE SURE TO READ THE WHOLE REQUEST! */
  if (-1 == (n = read(client, buffer, sizeof(buffer) - 1))) 
  {
    perror("Reading request");
    return -1;
  }
  
  buffer[n] = 0;

  /* Display the request */
  puts(buffer);

  char address[BUF_SIZE]; 
  //Parse request for address of content requested, check for GET. 

  int tryparse = parsehttp(buffer, address);
  if(tryparse != 1)
  {
      strcpy(buffer, 
  "HTTP/1.x 405 Method Not Allowed\n"
  "Content-Type: text/html\n\n"
  "Method not allowed.\n"); 
  }
  else if (0 == strcmp(address, "STOP"))
  {
    char donestr[] = "DONE.";
    strcpy(buffer, donestr);
    pthread_mutex_lock(&mutex); 
    current--; 
    pthread_mutex_unlock(&mutex);
    done = 1; 
  }
  else if (0 == strcmp(address, "STAT"))
  {
    pthread_mutex_lock(&mutex); 
    total++; 
    current ++; 
    pthread_mutex_unlock(&mutex);

    char statout[200]; 
    char currentstr[50];
    char totalstr[50];
    sprintf(currentstr, "%i", current);
    sprintf(totalstr, "%i", total);

    strcpy(statout, 
    "HTTP/1.x 200 OK\n"
    "Content-Type: text/html\n\n"
    "STATS PAGE\r\n\n"
     "CURRENT: ");
    strcat(statout, currentstr); 
    strcat(statout, "\r\n TOTAL: "); 
    strcat(statout, totalstr); 
    strcat(statout, "\r\n");
    strcpy(buffer, statout);
  }
  else if(strlen(address) > 0 ) // looking for a file. 
    {
      pthread_mutex_lock(&mutex); 
      total++; 
      current ++; 
      pthread_mutex_unlock(&mutex);

      FILE *fp; 
      char fulladdr[BUF_SIZE+4]; 
      strcpy(fulladdr, "///tmp/"); 
      strcat(fulladdr, address); 
      puts(fulladdr); 

      fp=fopen(fulladdr, "r");

      if (fp == NULL)
      {
        perror(fopen); 
        strcpy(buffer, 
        "HTTP/1.x 404 Not Found\n"
        "Content-Type: text/html\n\n"
        "404: Not Found\n");

      }
      else
      {
        strcpy(buffer, 
        "HTTP/1.x 200 OK\n"
        "Content-Type: text/html\n\n"); 

        char *buffer_ptr = buffer;
        while( !feof(fp) )
        {
          int numread = fread(buffer, sizeof(unsigned char), BUF_SIZE, fp);
          if( numread < 1 ) break; // EOF or error
          int numsent = send(client, buffer_ptr, numread, 0);
          if( numsent < 1 ) break;// 0 if disconnected, otherwise ERRORS 
        }
      }
    }
    else  //going to homepage, not requesting file? 
    {
      pthread_mutex_lock(&mutex); 
      total++; 
      current ++; 
      pthread_mutex_unlock(&mutex);

      strcpy(buffer, 
      "HTTP/1.x 200 OK\n"
      "Content-Type: text/html\n\n"
      "Welcome to localhost.\n");
    }

  /* Respond */
  n = write(client, buffer, strlen(buffer));

  if (-1 == n)  
  {
    perror("Writing response");
    return -1;
  }
  done = 1; 
  return 1; 
}




int parsehttp(char *httpbuffer, char filename[])
{ 
  char tempbuf[BUF_SIZE]; 
  char token1[BUF_SIZE],token2[BUF_SIZE], token3[BUF_SIZE], token4[BUF_SIZE] ;

  //check for GET 
  char check_get[4]; 
  if(strncpy(check_get, httpbuffer, 3))
    {
      int check = strcmp(check_get, "GET"); 
      if (check!= 0)
      {
        return -1;
      }
    } 


  const char slash="/";
  const char html = ".";

  strcpy(token1, strtok(httpbuffer, "/")); 
  if (token1 == NULL)
  {
    return -1; 
  }

  strcpy (token2, strtok(NULL," ")); 
   if (token2 == NULL)
  {
    return -1; 
  }

  //check if address is empty. if not, copy into filename. 
  char check_http[5]; 
  if(strncpy(check_http, token2, 4))
    {
      int check = strcmp(check_http, "HTTP"); //empty
      if (check == 0)
      {
        strcpy(filename, "");
        return 1;
      }
      else
      {
        //copy filename into array passed into func    
        strcpy(filename, token2); 
        if (filename == NULL)
        {
          perror(filename);
          return -1; 
        }
      }
    } 
  


  strcpy(token3, strtok(token1, "/"));
  if(token3 == NULL)
    {
    return -1;
    }
  //all good 
  return 1; 
}