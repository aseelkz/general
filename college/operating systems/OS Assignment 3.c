#include <stdlib.h> 
#include <stdio.h> 
#include <ctype.h>  
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>





int main(int argc, char* argv[])
{
	int n; //n is number of subprocesses
	FILE *fp; 

	struct timeval start, end; 
	gettimeofday(&start, NULL);
	int totalcount=0; 


	//checking argc, argv
	if(argc == 2){
		n = 1 ;
	}
	else if(argc == 3)
	{
		if ( atoi(argv[2]) && (argv[2]>0) ) 
		{
			n = atoi(argv[2]);
		}
		else
		{
			printf("Erorr: input not a positive number \n");
			exit(EXIT_FAILURE);
		} 

	}
	else
	{
		printf("Not enough inputs \n");
	}

	char * filename = (char *) malloc( strlen(argv[1])   +1   );	
	if( filename == NULL)
	{
		perror("malloc ");
	}
	strcpy(filename, argv[1]); 


	fp = fopen(filename, "r"); 
	if (fp == NULL) 
	{
		perror("fopen ");
	}


	//getting file size & chunk sizes for subprocess
	int fd; 
	struct stat st; 
	fd = fileno(fp);
	fstat(fd, &st);
	size_t filesize = st.st_size;
	{
		printf("filesize: %li \n",filesize); //CHECK if REM
	};
	
	int chunksize = filesize/n; 
	int i,checkpid, pipetochild[2], pipetoparent[2]; 
	int pid[n]; 

	for (i= 0; i<n;	i++)
	{

		if (pipe(pipetochild)== -1 || pipe(pipetoparent) == -1 )
			{   
			perror("pipe ");
			return(EXIT_FAILURE);
			}	

		checkpid = fork();

		int childcount = 0 ; 
		if(checkpid > 0)
		{

			close(pipetochild[0]);
			write(pipetochild[1], fp, chunksize);
			close(pipetochild[1]); 
			close(pipetoparent[1]); 
			read(pipetoparent[0], &childcount, sizeof(childcount));
			totalcount = totalcount + childcount; 
			close(pipetoparent[0]);
			if(i == n )
			{

				printf("Total number of lines in file: %d \n", totalcount);

			}
		}


		else if (checkpid ==0)
			{	//is child
				char buffer;
				pid[i] = getpid();  
				//printf ("I'm a child in pid[%d] with id %d \n", i,pid[i]); //REM
				
				size_t offset = chunksize*i; 
				if (lseek(fd,(off_t)offset,SEEK_SET) <0 )
				{
					perror("lseek ");
				}

				i = n+1; // to avoid recursion within child processes
				close(pipetochild[1]); 
				read(pipetochild[0], fp, chunksize);

				while((buffer = getc(fp)) != EOF)
				{
					if (buffer == '\n')
					{
						childcount++; 
					}
				}


				close(pipetochild[0]);
				close(pipetoparent[0]);
				write(pipetoparent[1], &childcount, sizeof(childcount)); 
				//printf("reporting: %d \n", childcount);
				close(pipetoparent[1]);
				exit(1);
			}		
	}


	for(i = 0; i < n; i++) {
        int status = 0;
        pid_t cpid = wait(&status);
        //printf("child %d has reported. \n", (int)cpid);   // checking child proc reports
    }


gettimeofday(&end, NULL);
printf("total lines : %d \n", totalcount);
printf("time elapsed: %ld\n", ((end.tv_sec * 1000000 + end.tv_usec)
	- (start.tv_sec * 1000000 + start.tv_usec)));

return(EXIT_SUCCESS);

}//end main