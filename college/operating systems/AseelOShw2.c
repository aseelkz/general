#include <stdlib.h>
#include <string.h>
#include <stdio.h> 
#include <dirent.h>
#include <ctype.h>
#include <string.h>
#include <pwd.h>

int main(int argc, char *argv[])
{
	
	DIR * 	dir; 
	struct dirent *d; 
	dir = opendir("/proc");
	int surpress; 
	if (argc>1 && strcmp(argv[1], "wo"))
	{
		surpress = 1;
	}

	if (dir)
	{
		//print coloumn titles 
		if (surpress==1)
		{
			printf("\n %6s  %6s  %2s %6s %21s \n","PID", "PPID", "STATE", "USER", "PROCESS NAME");
		}
		else
		{
			printf("\n %6s  %6s  %2s %6s %21s %10s \n ","PID", "PPID", "STATE", "USER", "PROCESS NAME","CMD ARGS");
		}


		while ((d = readdir(dir)) != NULL)
			//read through /proc subfolders
		{
			int check = 0; 
			for (int i = 0; i < strlen(d->d_name); i++)
				/*Go through all folders in /proc and check if numerical to use. */
			{
				if(isdigit(d->d_name[i]))
				{
					check = 1;
				}
				else
				{
					check = 0; 
					break; 
				}
			}//end forloop 
			if (check)
			{	
				//getting full file paths 
				int pid, ppid, uid;
				char state; 
				char comm[30], username[30], cmdarg[100], cws[30]; 
				int isread; 
				
				char *cmdloc = malloc((strlen("/proc/")+strlen(d->d_name)+strlen("/cmdline")+1));
				char *statusloc =malloc ((strlen("/proc/") + strlen(d->d_name)+strlen("/status")+1)); 
				char *fileloc = malloc((strlen("/proc/") + strlen(d->d_name) + strlen("/stat")+1));
				if (fileloc)
				{
					strcpy(fileloc, "/proc/");
					strcat(fileloc, d->d_name);
					strcat(fileloc, "/stat"); 

					strcpy(statusloc, "/proc/");
					strcat(statusloc, d->d_name);
					strcat(statusloc, "/status");

					strcpy(cmdloc, "/proc/");
					strcat(cmdloc, d->d_name);
					strcat(cmdloc, "/cmdline");

					//get data from /status and /cmdline 
					//and then from /stat and print each process

					FILE *statusfile = fopen(statusloc, "r");
					char sline[100], ws[4]; 
					
					while(fgets(sline, sizeof(sline), statusfile))
					{
						sscanf(sline, "Uid:%s%d%n", ws, &uid, &isread);
						struct passwd *pw = getpwuid(uid);
						if(pw != 0 )
						{
							strcpy(username, pw->pw_name);
						}
					}//endwhile

					
					if(surpress != 1)
					{
						FILE *cmdfile = fopen(cmdloc,"r");
						char cline[100];
						int beenread;
						while(fgets(cline, sizeof(cline),cmdfile))
						{	
							sscanf(cline, " %[^\n]%s%c%c", cws, "\0","\0", &beenread);
						}//endwhile
					}//if -y is passed, to print 'cmdline arg' column



					FILE *file = fopen(fileloc, "r");
					char line[400];

					while(fgets(line, sizeof(line), file))
					{
						sscanf(line, "%d %s %c %d", &pid, &comm, &state, &ppid);

						if(surpress == 1)
						{
							printf("%6d  %6d  %2c %12s %25s \n",pid, ppid, state, username, comm);
						}
						else
						{
							printf("%6d  %6d  %2c %12s %25s %20s\n",pid, ppid, state, username, comm,cws);	
						}
					}//end while (printing)
				


				}
				else 
				{
					printf("%s \n","Unable to allocate memory." );
					return(EXIT_FAILURE);
				}

			}// end iffiloc 
		}//end while readdir
	
	}

return(EXIT_SUCCESS)	; 

}