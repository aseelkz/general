	$national_number_codes = [
		'Bahraini' => 9,
		'Saudi Arabian' => 10,
		'Emirati' => 15,
		'Qatari' => 11,
		'Kuwaiti' => 12, 
		'Omani' => 8, 
	];

	$nationality = Request::get(key:'nationality');
	
	$number_of_allowed_digits = $national_number_codes[$nationality];
	$number_of_digits = count_digits($value);
	is_valid = ($number_of_allowed_digits == $number_of_digits); 

	return $is_valid;