#include <stdlib.h> 
#include <stdio.h> 
#include <ctype.h>  
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>





int main(int argc, char* argv[])
{
	int n; //number of child processes 
	int totalcount=0;  


	//checking argc, argv for correct inputs..
	if(argc == 2)
	{
		n = 1 ;
	}
	else if(argc == 3)
	{ 
		char* endpt; 
		int isint = strtol(argv[2], &endpt, 10);
		if ( ! *endpt)    
		{ 
			n = isint;
		}
		else
		{
			printf("Erorr: input not a positive number \n"); 
			exit(EXIT_FAILURE);
		} 
	}
	else
	{
		printf("Not enough inputs \n"); 
		exit(EXIT_FAILURE);
	}

	char * filename = argv[1];
	int fd; 

	fd = open(filename, O_RDONLY); //opening file..
	if (fd < 0 ) 
	{
		perror("filename ");
		exit(EXIT_FAILURE);
	}


	//getting file size & chunk sizes for subprocess
	struct stat st; 
	fstat(fd, &st);
	size_t filesize = st.st_size;
	
	int chunksize = filesize/n; 
	
	int i,checkpid; 
	int pipes[2*n][2];

	struct timeval start, end; 
	gettimeofday(&start, NULL); 

	for (i= 0; i<n;	i++)
	{

		//opening pipes.. pipe[j] = to child,  pipe[j+1] to parent... 
		//p[ ][0]= read end, p[ ][1] = write end 
		int j=2*i;
		if (pipe(pipes[j])== -1 || pipe(pipes[j+1]) == -1 )
			{   
			perror("pipe ");
			return(EXIT_FAILURE);
			}	

		checkpid = fork();
		pid_t wpid; 
		int status=0; 
		int childcount = 0 ;

		if(checkpid > 0) //parent proc
		{
			char * bufferin; 
			//close unneded pipe sides, write fp

			close(pipes[j][0]);
			write(pipes[j][1], &bufferin, chunksize);
			close(pipes[j][1]); 
			close(pipes[j+1][1]); 


			//wait here fro status
			while ((wpid=wait(&status))>0);

			//get count from child
			if(read(pipes[j+1][0], &childcount, sizeof(childcount)) <= 0)
				{
					perror("read"); 
					exit(EXIT_FAILURE);
				}
			totalcount = totalcount + childcount; 
			write(pipes[j][1], &checkpid, sizeof(int));
			close(pipes[j][1]);
			close(pipes[j+1][0]);

		}//end ifparent


		else if (checkpid ==0) //child proc
			{	
				char bufferout;
				int maxcount =0; 
				size_t offset = chunksize*i; //offset where to start reding
				if (lseek(fd,(off_t)offset,SEEK_SET) <0 )
				{
					perror("lseek ");
				}

				close(pipes[j][1]); 


				//read by char, check for newline
				while(read(fd, &bufferout, 1)>0  && (maxcount < chunksize))
				{
					if(bufferout == '\n')
					{
						childcount++;
					}
					maxcount++; 
				}


				//close pipes, rport # of lines
				close(pipes[j][0]);
				close(pipes[j+1][0]);
				write(pipes[j+1][1], &childcount, sizeof(childcount)); 
				close(pipes[j+1][1]);
				exit(1);
			}//end ifchild		
	}//end loop-n


gettimeofday(&end, NULL);
printf("total lines : %d \n", totalcount);
fprintf(stderr,"time elapsed: %ld\n", ((end.tv_sec * 1000000 + end.tv_usec)
	- (start.tv_sec * 1000000 + start.tv_usec)));

return(EXIT_SUCCESS);

}//end main